
#ifndef HASH_TABLE
#define HASH_TABLE

#ifdef __cplusplus
extern "C" {
#endif


#include <stdint.h>

#define PRIME_MULTIPLIER 31

// TODO: I think an initial implementation should hash some 
struct HashTable
{
    uint32_t tableSize;

    char** values;
    // TODO: Have an array of linked lists with string values instead of char** values. NOTE: This will take up more memory, lots of pointers being stored
    //              UTILIZE union TO ALLOW FOR MULTIPLE TYPES, INCLUDE A TYPE ENUM AND PARAMETER IN CreateHashTable (see queue.h for an example)
};

static inline uint32_t ComputeHash( char* key, uint16_t keyLength )
{
    uint32_t hash;

    // TODO: Assuming character exists. May want to know length of string...
    for( uint16_t charIterator = 0; charIterator < keyLength; ++charIterator )
    {
        hash = (hash * PRIME_MULTIPLIER) + key[charIterator]; // Multiply by a prime number to reduce chance of collision (since prime numbers have no factors and therefore less likely pattern to emerge in key hashes)
    }

    return hash;
}

// TODO: Handle collisions, LinkedList is the approach I am currently desiring since it will allow for more keys than slots
//       But, I'm thinking it may be better to have a strong hash computing function and just don't store collisions... But for a hashtable of small size (e.g. 10) this may lead to many collisions... Use Open Addressing?
static inline void CheckAndHandleCollision( struct HashTable* table, 
                                            uint32_t index, char* value )
{
    if( table->values[index] ) // Collision
    {
        // TODO: Utilize Open Addressing to handle collision (how implement? Linear Probing, Quadratic Probing, or Double Hashing? Double Hashing I believe has been said to be the best.. Revisit notes)
    }
    else
    {
        table->values[index] = value;
    }
}




struct HashTable CreateHashTable( uint32_t tableSize );

void HashTableInsert( struct HashTable* table, char* key, uint16_t keyLength, char* value );
char* HashTableLookup( struct HashTable* table, char* key, uint16_t keyLength );

void DestroyHashTable( struct HashTable* table );


#ifdef __cplusplus
}
#endif

#endif // HASH_TABLE

