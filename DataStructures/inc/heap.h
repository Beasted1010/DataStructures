
#ifndef HEAP_H
#define HEAP_H

// TODO: Future Considerations (learning-value order)
//          - Priority Queues (for both Max and Min heaps)
//          - MinHeap

#ifdef __cplusplus
extern "C" {
#endif

#define LOG_VERIFY( ... ) printf("VERIFYING: "); printf( __VA_ARGS__ ); printf("\n");

struct Heap
{
    int heapSize;

    int* items;
    int itemCount;
};


// Max Heap Functions
struct Heap CreateMaxHeap( int* array, int arrayLength );

void MaxHeapify( int index, int* array, int arrayLength );
void MaxHeapSort( int* array, int arrayLength );

// TODO: Implement these functions
void CreateMinHeap();
void MinHeapify();


// Heap Functions
void DestroyHeap( struct Heap* heap );
void PrintHeap( struct Heap* heap );

// Tests
void VerifyCreateMaxHeap( int* array, int arrayLength );
void VerifyHeapSort( int* array, int arrayLength );


#ifdef __cplusplus
}
#endif

#endif // HEAP_H
