
#ifndef ARRAY_BUILDER_H
#define ARRAY_BUILDER_H

#include <stdint.h>


void FillRandomIntArrayInRange( int* array, uint32_t arrayLength, int min, int max ); // Inclusive both ends of range
void FillRandomIntArray( int* array, uint32_t arrayLength );


#endif // ARRAY_BUILDER_H

