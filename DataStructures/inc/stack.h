

#ifndef STACK_H
#define STACK_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdlib.h>
#include <stdio.h>



#ifndef TYPE
#define TYPE
    typedef enum TypeEnum
    {
        NOTHING,
        INT,
        REAL,
        CHAR
    } Type;
#endif

typedef struct StackStruct
{
    union
    {
        int* top_i;
        char* top_c;
    };

    int size;
    int used;

    Type type;

} Stack;


Stack CreateStack( Type type, int size )
{
    Stack stack;

    stack.size = size;
    stack.used = 0;

    stack.type = type;

    switch( stack.type )
    {
        case INT:
        {
            stack.top_i = (int*) malloc( sizeof(int) * stack.size );
        } break;

        case CHAR:
        {
            stack.top_c = (char*) malloc( sizeof(char) * stack.size );
        } break;

        default:
        {

        }
    }

    return stack;
}

void DeleteStack( Stack* stack )
{
    switch( stack->type )
    {
        case INT:
        {
            free( stack->top_i );
        } break;

        case CHAR:
        {
            free( stack->top_c );
        } break;
    }

    stack->size = 0;
    stack->type = NOTHING;
}


void Push_i( Stack* stack, int val )
{
    if( stack->used < stack->size )
    {
        // TODO: Make this not leave an empty first element (we pass over the first element)
        stack->top_i++; // Want to do pointer arithmetic (move "up" a size of int)
        *(stack->top_i) = val;
        stack->used++;
    }
}

void Push_c( Stack* stack, char val )
{
    if( stack->used < stack->size )
    {
        // TODO: Make this not leave an empty first element (we immediately pass over the first element)
        stack->top_c++;  // Want to do pointer arithmetic (move "up" a size of char)
        *(stack->top_c) = val;
        stack->used++;
    }
}


int Pop_i( Stack* stack )
{
    int result = *(stack->top_i);

    stack->top_i--;

    stack->used--;

    return result;
}

char Pop_c( Stack* stack )
{
    char result = *(stack->top_c);

    stack->top_c--;

    stack->used--;

    return result;
}

void StackPeek( Stack* stack )
{
    switch( stack->type )
    {
        case INT:
        {
            int result = Pop_i(stack);
            Push_i( stack, result );
            printf("Top of Stack: %i\n", result );
        } break;

        case CHAR:
        {
            char result = Pop_c(stack);
            Push_c( stack, result );
            printf("Top of Stack: %c\n", result );
        } break;
    }
}


// TODO: I rethought this halfway thorugh... So finish if desired... :D
void PrintStack( Stack* stack )
{
    int used = stack->used;

    for( int i = 0; i < used; i++ )
    {
        switch( stack->type )
        {
            case INT:
                printf("%i ", Pop_i(stack) );
                break;

            case CHAR:
                printf("%c ", Pop_c(stack) );
                break;
        }
    }
}


#ifdef __cplusplus
}
#endif

#endif //STACK_H
