
#ifndef GRAPH_H
#define GRAPH_H

#ifdef __cplusplus
extern "C" {
#endif

// TODO: ADD ERROR HANDLING (e.g. mallocs)
// TODO: ADD GraphType FUNCTIONALITY (i.e. enforce particular graph types as new nodes are added)
// TODO: IMPROVE THE INTERFACE FOR ADDING NODES AND ADDING ADJACENT NODES. Ultimately just make the usability better...

#include <stdint.h>

#ifndef TRUE
#define TRUE 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

enum GraphType
{
    DISCONNECTED_DIGRAPH = 0, // The thing that is none of the below (i.e. cyles allowed, directed, isolated subgraphs allowed)
    ACYCLIC = 0b0001, // Don't allow cycles (i.e. enforce during any node insertions)
    UNDIRECTED = 0b0010, // Two-way paths (i.e. create copies in adjacency list for every add)
    WEAKLY_CONNECTED = 0b0100, // No isolated subgraphs (i.e. enforce all pairs of nodes have an UNDIRECTED path between)
    STRONGLY_CONNECTED = 0b1100 // No isolated subgraphs (i.e. enforce all pairs of nodes have a DIRECTED path between)
};

struct Node
{
    const char* name;

    struct Node* adjacentNodes;
    uint32_t adjacentNodeCount;

    uint8_t visited;
};

struct Graph
{
    const char* name;

    enum GraphType type;

    struct Node* nodes;
    uint32_t nodeCount;
};

struct Node CreateNode( const char* name, struct Node* adjacentNodes, uint32_t adjacentNodeCount );
void AddAdjacentNodeToNode( struct Node* adjacentNode, struct Node* node );
void PrintNode( struct Node* node );
void DestroyNode( struct Node* node );


struct Graph CreateGraph( const char* name, enum GraphType type );
void AddNodeToGraph( struct Node* nodeToAdd, struct Graph* graph );
struct Node CreateAndAddNodeToGraph( const char* name, struct Node* adjacentNodes, uint32_t adjacentNodeCount, struct Graph* graph );
void AddAdjacentNodeToNodeInGraph( struct Node* adjacentNode, struct Node* nodeToAddTo, struct Graph* graph );
struct Node* FindNodeByNameInGraph( const char* name, struct Graph* graph );
void DepthFirstTraversalFromNode( struct Node* node );
void DepthFirstTraversalOfGraph( struct Graph* graph );
void BreadthFirstTraversalOfGraph( struct Graph* graph );
void PrintGraph( struct Graph* graph );
void DestroyGraph( struct Graph* graph );



// Tests

void VerifyCreateNode();

void VerifyCreateGraph();
void VerifyCreateAndAddNodeToGraph( struct Graph* graph );
void VerifyFindNodeByNameInGraph();
void VerifyDepthFirstTraversal( struct Graph* graph );

void VerifyGraph( enum GraphType type );


#ifdef __cplusplus
}
#endif


#endif // GRAPH_H

