
#ifndef LINKED_LIST_H
#define LINKED_LIST_H

#ifdef __cplusplus
extern "C" {
#endif


#include <stdio.h>
#include <stdlib.h>


/* IMPLEMENTATION NOTES
    This file implements a LINKED LIST
    Currently support a Double Linked List only (can be used a single linked list I suppose)


    NOTE's:
        - During calls to malloc I am casting the void* so that the code is compatible with C++
        - Node identifier (node->id) is 1-indexed


    TODO's:
        - Support circular linked list
        - Support single linked list
        - Optimize linked list type memory allocation for node
        - Make a CreateNode function separate from AddAndCreateNode
        - Move function definitions into .c file
        - The CIRCULAR List Type should support single or double as well... Implement as bit field?
        - Allow user to create node in user space and pass in node to be added to linked list
        - Functions associated with flag, such as PrintNode_Flag( int desiredFlag ), SetFlag -> One node, SetFlags -> Mult nodes
            - Maybe use a bit field of some sort to allow for efficient storage of multiple flags (with meaning to each section)?

    BUG's:
        - Some of my mallocs are not necessary and even create garbage... i.e. I set aside space, then have pointer point elsewhere


    IDEA's:
        - Give a linked list a named identifier

*/


// This enum may be defined in my other data types (e.g. stack)
#ifndef TYPE
#define TYPE
    typedef enum TypeEnum
    {
        NOTHING,
        INT,
        REAL,
        STRING,
        CHAR
    } Type;
#endif

// The enum representing the type of linked list
typedef enum ListTypeEnum
{
    SINGLE,
    DOUBLE,
    CIRCULAR
} ListType;

// The union representing the possible data a node may contain
typedef union NodeDataUnion
{
    // Making these pointers with the idea that I may want the node to hold multiple of that type
    int data_i;
    char data_c;
    char* data_s;
    double data_r;
} NodeData;

// Enum for possible flag values
typedef enum{ false, true } Flag;

// Forward declartion of Node
typedef struct NodeStruct Node;

// The struct representing a Node
struct NodeStruct
{
    Type type;

    // A flag to be used as user desires
    Flag flag;

    // The data the node will contain
    NodeData data;

    int id;

    // TODO: Optimize this so that if we have a single linked list we aren't allocating space for a whole other node
    // Support linkage of nodes with next and prev
    Node* next;
    Node* prev;
};

// The struct representing our Linked List
typedef struct LinkedListStruct
{
    Node* head;
    Node* tail;

    ListType type;
    int numNodes;

} LinkedList;

// FUNCTIONS

// Create a linked list given a linked list type
LinkedList* CreateLinkedList( ListType listType );

// Add a node with given type and data to the END of the specified linked list
Node CreateAndAddNode( LinkedList* list, Type type, NodeData data, Flag flag );

// Add a node to the linked list that is already apart of another linked list
// Return: The id of the added node
int AddNode( LinkedList* list, Node* node );

// Delete a node with the given node count from the specified linked list
void DeleteNode( LinkedList* list, int node );

// Print all nodes (in order) in the specified linked list
void PrintLinkedList( LinkedList* list );

// Delete the specified linked list
void DeleteLinkedList( LinkedList** list );

// A function to grab the ID of a node based on data contained by the node, supports a primitive verison of multiple occurances
// Also supports checking if linked list does not contain a specific piece of data
int GetNodeID( LinkedList* list, NodeData data, Type type, int occurance );

// A function to allocate memory for a node
void AllocateNode( Node** node );

// A function to deallocate memory for a node
void DeallocateNode( Node** node );

// A function to DEEP copy node content from node to node
void CopyNode( Node* copyFrom, Node* copyTo );

// A function to print the data contained in a node
void PrintNode( Node* node );

// Delete Node based on data
// Delete Node based on count
// Print Node based on data?
// Print Node based on count?
// AddSharedNode? -> Need pointer to pointer for all nodes... Probably, at least head/tail, prev/next...

// TODO: Move the below function definitions into .c file

// Create a linked list given a linked list type
LinkedList* CreateLinkedList( ListType listType )
{
    LinkedList* list = (LinkedList*) malloc( sizeof(LinkedList) );

    if( listType != SINGLE && listType != DOUBLE && listType != CIRCULAR )
    {
        printf("Can't create Linked List. Specified Linked List type is not supported!\n");
        return NULL;
    }

    if( !list )
    {
        printf("Allocation of Linked List during creation failed!\n");
        exit(1);
    }

    list->type = listType;

    list->head = (Node*) malloc( sizeof(Node) );

    if( !list->head )
    {
        printf("Allocation of Head Node during Linked List creation failed!\n");
        exit(1);
    }

    list->tail = (Node*) malloc( sizeof(Node) );

    if( !list->tail )
    {
        printf("Allocation of Tail Node during Linked List creation failed!\n");
        exit(1);
    }

    list->head = NULL;
    list->tail = NULL;

    list->numNodes = 0;

    return list;
}

// Add a node with given type and data to the END of the specified linked list
// Return: The newly created and added node
Node CreateAndAddNode( LinkedList* list, Type type, NodeData data, Flag flag )
{
    Node* node = (Node*) malloc( sizeof(Node) );

    if( !node )
    {
        printf("Allocation of Node during creation failed!\n");
        exit(1);
    }

    node->type = type;

    /*switch( node->type )
    {
        case INT:
        {
            node->data.data_i = data.data_i;
        } break;

        case REAL:
        {
            node->data.data_r = data.data_r;
        } break;

        case CHAR:
        {
            node->data.data_c = data.data_c;
        } break;

        case STRING:
        {
            node->data.data_s = data.data_s;
        } break;

        default:
        {
            printf("The Node you are trying to create in linked list has an invalid type!!\n");
            exit(1);
        } break;
    }*/
    node->data = data;
    node->flag = flag;

    node->next = (Node*) malloc( sizeof(Node) );

    if( !node->next )
    {
        printf("Failed to allocate memory for node->next!\n");
        exit(1);
    }

    node->prev = (Node*) malloc( sizeof(Node) );

    if( !node->prev )
    {
        printf("Failed to allocate memory for node->prev!\n");
        exit(1);
    }

    node->next = NULL;
    node->prev = NULL;

    // Is the list non-empty?
    if( list->numNodes > 0 )
    {
        // Set the new node's previous to the current tail
        node->prev = list->tail;

        // We have a new tail
        list->tail = node;

        // Since we now have at least 2 nodes, set the previous node's next to the new node
        Node* temp = node->prev;
        temp->next = node;
    }
    else // The first node in list
    {
        list->head = node;
        list->tail = node;
    }


    // We are adding a node, increment the node counter
    // Also, give our new node an identifier (1-indexed)
    node->id = ++list->numNodes;

    return *node;
}

// Add a node to the linked list that is already apart of another linked list
// Return: The id of the added node
int AddNode( LinkedList* list, Node* node )
{
    // TODO: Add error handling cases (e.g. does this node have data?)

    // 2 cases: First node is list. Last node in list.

    Node* helper = list->tail;

    Node* newNode;
    AllocateNode(&newNode);

    CopyNode( node, newNode );

    // Does the list have any nodes?
    if( list->numNodes > 0 )
    {
        // Adjust list for new node
        helper->next = newNode;
        list->tail = newNode;
        (list->tail)->prev = helper;

        (list->tail)->next = NULL;
    }
    else // List is empty
    {
        list->head = newNode;
        list->tail = newNode;
    }

    // We are adding a node, increment the node counter
    // Also, give our new node an identifier (1-indexed)
    newNode->id = ++list->numNodes;

    return newNode->id;
}

// A function to print the data contained in a node
void PrintNode( Node* node )
{
    switch( node->type )
    {
        case INT:
        {
            printf("Node %i contains data: %i\n", node->id, node->data.data_i);
        } break;

        case REAL:
        {
            printf("Node %i contains data: %f\n", node->id, node->data.data_r);
        } break;

        case CHAR:
        {
            printf("Node %i contains data: %c\n", node->id, node->data.data_c);
        } break;

        case STRING:
        {
            printf("Node %i contains data: %s\n", node->id, node->data.data_s);
        } break;

        default:
        {
            printf("Node %i in linked list has an invalid type! ...Skipping!\n", node->id);
        } break;
    }
}

// Print all nodes (in order) in the specified linked list
void PrintLinkedList( LinkedList* list )
{
    // Start the head of the linked list
    Node* node = list->head;

    printf("Linked list contains %i nodes!\n", list->numNodes);

    // Traverse linked list and print out value of each node
    for( int i = 0; i < list->numNodes; i++ )
    {
        printf("%i:", node->id);
        switch( node->type )
        {
            case INT:
            {
                printf("%i ", node->data.data_i);
            } break;

            case REAL:
            {
                printf("%f ", node->data.data_r);
            } break;

            case CHAR:
            {
                printf("%c ", node->data.data_c);
            } break;

            case STRING:
            {
                printf("%s ", node->data.data_s);
            } break;

            default:
            {
                printf("Node %i in linked list has an invalid type! ...Skipping!\n", node->id);
            } break;
        }

        if( node->next )
            node = node->next;
    }

    printf("\n");
}

// A function to grab the ID of a node based on data contained by the node, supports a primitive verison of multiple occurances
// Also supports checking if linked list does not contain a specific piece of data

// TODO: BROKEN! FIX!
int GetNodeID( LinkedList* list, NodeData data, Type type, int occurance )
{
    int occuranceCount = 0;

    // Start at the head of the linked list
    Node* node = list->head;

    for( int i = 0; i < list->numNodes; i++ )
    {
        // Did we find an occurance of the data?
        switch( type )
        {
            case INT:
            {
                if( node->data.data_i == data.data_i );
                    occuranceCount++;
            } break;

            case REAL:
            {
                if( node->data.data_r == data.data_r );
                    occuranceCount++;
            } break;

            case CHAR:
            {
                if( node->data.data_c == data.data_c );
                    occuranceCount++;
            } break;

            case STRING:
            {
                if( node->data.data_s == data.data_s );
                    occuranceCount++;
            } break;

            default:
            {
                printf("Provided data type is invalid!\n");
            } break;
        }

        // We found the appropriate number of occurances of the data! This is the node we want.
        if( occuranceCount == occurance )
            break;

        // Ensure we have a next node
        if( node->next )
            node = node->next;
    }

    if( occuranceCount != occurance )
    {
        switch( type )
        {
            case INT:
            {
                printf("The linked list does not contain %i occurances of data: %i\n", occurance, node->data.data_i);
            } break;

            case REAL:
            {
                printf("The linked list does not contain %i occurances of data: %f\n", occurance, node->data.data_r);
            } break;

            case CHAR:
            {
                printf("The linked list does not contain %i occurances of data: %c\n", occurance, node->data.data_c);
            } break;

            case STRING:
            {
                printf("The linked list does not contain %i occurances of data: %s\n", occurance, node->data.data_s);
            } break;

            default:
            {
                printf("Provided data type is invalid!\n");
            } break;
        }
        return -1;
    }

    return node->id;
}

// A helper function to test if the linked list is empty
static inline int IsEmpty( LinkedList* list )
{
    if( list->numNodes == 0 )
    {
        printf("The Linked List contains no nodes. Nothing to delete!\n");
        return 1;
    }

    return 0;
}

// TODO: Use me throughout
// A function to allocate memory for a node
void AllocateNode( Node** node )
{
    *node = malloc( sizeof(Node) );

    if( !(*node) )
    {
        printf("Allocation of node failed.\n");
        exit(1);
    }

    (*node)->next = malloc( sizeof(Node) );

    if( !(*node)->next )
    {
        printf("Allocation of next node failed.\n");
        exit(1);
    }

    (*node)->next = NULL;

    (*node)->prev = malloc( sizeof(Node) );

    if( !(*node)->prev )
    {
        printf("Allocation of prev node failed.\n");
        exit(1);
    }

    (*node)->prev = NULL;
}

// A function to deallocate memory for a node
void DeallocateNode( Node** node )
{
    (*node)->next = NULL;
    (*node)->prev = NULL;
    free( (*node)->next );
    free( (*node)->prev );

    *node = NULL;

    free( *node );
}

// TODO: Fix the copy of nodes thing! IT SUCKS AND IS BROKEN
// A helper function to DEEP copy node content from node to node
void CopyNode( Node* copyFrom, Node* copyTo )
{
    if( !copyFrom )
    {
        printf("Node to copy from has not been allocated!\n");
        exit(1);
    }

    /*if( copyTo )
        DeallocateNode( &copyTo );

    AllocateNode( &copyTo );*/

    copyTo->prev = copyFrom->prev;
    copyTo->next = copyFrom->next;
    copyTo->type = copyFrom->type;
    copyTo->flag = copyFrom->flag;

    /*switch( copyFrom->type )
    {
        case INT:
        {
            copyTo->data.data_i = copyFrom->data.data_i;
        } break;

        case REAL:
        {
            copyTo->data.data_r = copyFrom->data.data_r;
        } break;

        case CHAR:
        {
            copyTo->data.data_c = copyFrom->data.data_c;
        } break;

        case STRING:
        {
            copyTo->data.data_s = copyFrom->data.data_s;
        } break;

        default:
        {
            printf("Provided data type is invalid!\n");
        } break;
    }*/
    copyTo->data = copyFrom->data;

    copyTo->id = copyFrom->id;
}

// A helper function to adjust our node IDs of the linked list
static inline void AdjustNodeIDs( LinkedList* list )
{
    // Create a new node
    Node* node;
    AllocateNode( &node );

    // We want to start at the head
    //CopyNode( list->head, node );
    node = list->head;
    int id;

    for( int i = 0; i < list->numNodes; i++ )
    {
        id = i + 1;
        node->id = id;

        if( node->next )
            node = node->next;
    }
}

// Delete a node with the given node count from the specified linked list
void DeleteNode( LinkedList* list, int nodeID )
{
    if( IsEmpty(list) )
        return;

    // Are we trying to delete a node that doesn't exist?
    if( nodeID > list->numNodes || nodeID < 1 )
    {
        printf("Specified node (using node count) does not exist!\n");
        printf("No node %i in the list!\n", nodeID);
        exit(1);
    }

    // Start at the head of our list
    Node* node = list->head;;
    //AllocateNode( &node );

    // Traverse our linked list until we come across the desired node to delete
    for( int i = 0; i < list->numNodes; i++ )
    {
        if( nodeID == node->id )
            break;

        if( node->next )
            node = node->next;
        else
        {
            printf("No next node in deletion of node... Something is wrong with this code!\n");
            printf("I should have come across the node ID by now!\n");
            exit(1);
        }
    }

    // Adjust the linked list to neglect the to be deleted node
    if( list->numNodes > 1 )
    {

        if( node->prev )
        {
            if( node->next)
            {
                (node->prev)->next = (node->next);
                (node->next)->prev = node->prev;
            }
            else
            {
                list->tail = node->prev;
                (list->tail)->next = NULL;
            }
        }
        else if( node->next ) // Ensure we have a following node (we should... due to numNodes > 1 & !node->prev)
        {
            list->head = node->next;
            (list->head)->prev = NULL;
        }
    }
    else // We are deleting the only node in the linked list
    {
        // node->next and node->prev should both be NULL already
        list->head = NULL;
        list->tail = NULL;
    }

    // The linked list contains 1 less node
    list->numNodes--;

    AdjustNodeIDs( list );

    // Free the memory
    DeallocateNode( &node );
}

// Delete the specified linked list
void DeleteLinkedList( LinkedList** list )
{
    if( IsEmpty((*list)) )
        return;

    // Start at the head of our list
    Node* node = (*list)->head;

    // Node to mark which to delete next
    Node* nodeReaper;

    int numNodes = (*list)->numNodes;
    // Traverse linked list deleting all nodes
    for( int i = 0; i < numNodes; i++ )
    {
        // Is there another node to delete?
        if( node->next )
            nodeReaper = node->next;

        DeleteNode( (*list), node->id );

        node = nodeReaper;
    }

    (*list)->head = NULL;
    (*list)->tail = NULL;

    free( (*list)->head );
    free( (*list)->tail );

    (*list)->type = NOTHING;

    (*list) = NULL;

    free((*list));
}


#ifdef __cplusplus
}
#endif

#endif // LINKED_LIST_H
