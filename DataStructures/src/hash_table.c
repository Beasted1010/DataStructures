
#include <stdlib.h>
#include <stdio.h>

#include "hash_table.h"

struct HashTable CreateHashTable( uint32_t tableSize )
{
    struct HashTable table;

    table.tableSize = tableSize;

    table.values = malloc( sizeof(char*) * tableSize );

    return table;
}


void HashTableInsert( struct HashTable* table, char* key, uint16_t keyLength, char* value )
{
    uint32_t index = ComputeHash( key, keyLength );

    CheckAndHandleCollision( table, index, value );

    printf("Inserted %s at index %i\n", table->values[index], index);
}

char* HashTableLookup( struct HashTable* table, char* key, uint16_t keyLength )
{
    uint32_t index = ComputeHash( key, keyLength );

    return table->values[index];
}


void DestroyHashTable( struct HashTable* table )
{
}



