
#include <stdio.h>
#include <stdlib.h>

#include "array_builder.h"

#include "hash_table.h"
#include "heap.h"

#include "graph.h"


// You either do a stress test or not (i.e. stress XOR normal test)
#define STRESS_TEST 0 // NOTE: Stress test will also stress your CPU (: It will print out all the values to the console and may take some time... BEWARE. TODO: Consider a better way (e.g. no print, print extremes only, etc.)
#define STRESS_TEST_SIZE 100000
#define NORMAL_TEST_SIZE 20

#define TEST_HASH_TABLE 0
#define TEST_HEAP 0
#define TEST_GRAPH 1


int main( int argc, char** argv )
{
#if STRESS_TEST 
    #define TEST_ARRAY_LENGTH STRESS_TEST_SIZE
#else
    #define TEST_ARRAY_LENGTH NORMAL_TEST_SIZE
#endif


#if TEST_HASH_TABLE
    struct HashTable table = CreateHashTable( 100 );

    char* key = "Key";
    char* value = "Value";

    HashTableInsert( &table, key, value );
    char* retrieved_value = HashTableLookup( &table, key );

    printf("Received value for %s = %s\n", key, retrieved_value);
#endif

#if TEST_HEAP
    int* test_array = (int*) malloc( sizeof(int) * TEST_ARRAY_LENGTH );

    FillRandomIntArray( test_array, TEST_ARRAY_LENGTH );
    //FillRandomIntArrayInRange( test_array, TEST_ARRAY_LENGTH, 0, 5 );
    
    VerifyCreateMaxHeap( test_array, TEST_ARRAY_LENGTH );
    VerifyHeapSort( test_array, TEST_ARRAY_LENGTH );
#endif


#if TEST_GRAPH
    
    VerifyGraph( DISCONNECTED_DIGRAPH );

#endif
    


    printf("End of tests\n");
    return 0;
}

