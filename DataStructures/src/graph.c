
#include <stdio.h>
#include <stdlib.h>

#include "graph.h"

#include "queue.h" // Used for BreadthFirstSearch

// TODO: Utilize my library version of these utilities

uint32_t StringLength( const char* str )
{
    if( !str )
    {
        return 0;
    }

    uint32_t counter = 0;

    while( str[counter] != '\0' )
    {
        ++counter;
    }

    return counter;
}

int8_t StringCompare( const char* str1, const char* str2 )
{
    uint32_t str1_length = StringLength( str1 );
    uint32_t str2_length = StringLength( str2 );

    for( uint32_t charIterator = 0; charIterator < str1_length && charIterator < str2_length ; ++charIterator )
    {
        if( str1[charIterator] != str2[charIterator] )
        {
            return (str1[charIterator] - str2[charIterator]); // Positive if str1[i] > str2[i], negative if str1[i] < str2[i]
        }
    }

    return 0;
}







struct Node CreateNode( const char* name, struct Node* adjacentNodes, uint32_t adjacentNodeCount )
{
    struct Node node;

    node.name = name;
    node.visited = FALSE;

    node.adjacentNodes = (struct Node*) malloc( sizeof(struct Node) * adjacentNodeCount );

    if( !node.adjacentNodes )
    {
        // TODO: ERROR HANDLE (my own logger?)
    }

    // WRONG node.adjacentNodes = adjacentNodes;
    node.adjacentNodeCount = adjacentNodeCount;

    return node;
}

void AddAdjacentNodeToNode( struct Node* adjacentNode, struct Node* node )
{
    node->adjacentNodes = (struct Node*) realloc( node->adjacentNodes, sizeof(struct Node) * (node->adjacentNodeCount+1) );
    node->adjacentNodes[node->adjacentNodeCount] = *adjacentNode;
    ++node->adjacentNodeCount;
}

void PrintNode( struct Node* node )
{
    printf("%i node(s) adjacent to %s: ", node->adjacentNodeCount, node->name);
    for( uint32_t adjacentNodeIterator = 0; adjacentNodeIterator < node->adjacentNodeCount; ++adjacentNodeIterator )
    {
        printf("%s  ", node->adjacentNodes[adjacentNodeIterator].name);
    }
    printf("\n\n");
}

void DestroyNode( struct Node* node )
{
    free( node->adjacentNodes );
    node->adjacentNodes = 0;

    node->adjacentNodeCount = 0;
}



struct Graph CreateGraph( const char* name, enum GraphType type )
{
    return (struct Graph) { .name=name, .type=type, .nodes=0, .nodeCount=0 };
}

void AddNodeToGraph( struct Node* nodeToAdd, struct Graph* graph )
{
    graph->nodes = (struct Node*) realloc( graph->nodes, sizeof(struct Node) * (graph->nodeCount+1) );

    if( !graph->nodes )
    {
        // TODO: ERROR HANDLE (my own logger?)
    }

    graph->nodes[graph->nodeCount] = *nodeToAdd;

    ++graph->nodeCount;
}

struct Node CreateAndAddNodeToGraph( const char* name, struct Node* adjacentNodes, uint32_t adjacentNodeCount, struct Graph* graph )
{
    graph->nodes = (struct Node*) realloc( graph->nodes, sizeof(struct Node) * (graph->nodeCount+1) );

    if( !graph->nodes )
    {
        // TODO: ERROR HANDLE (my own logger?)
    }

    graph->nodes[graph->nodeCount] = CreateNode( name, adjacentNodes, adjacentNodeCount );

    ++graph->nodeCount;

    return graph->nodes[graph->nodeCount-1];
}

void AddAdjacentNodeToNodeInGraph( struct Node* adjacentNode, struct Node* nodeToAddTo, struct Graph* graph )
{
    struct Node* node = FindNodeByNameInGraph( nodeToAddTo->name, graph );

    if( !node )
    {
        // Handle NodeNotFound WARNING

        return;
    }

    AddAdjacentNodeToNode( adjacentNode, node );

}

struct Node* FindNodeByNameInGraph( const char* name, struct Graph* graph )
{
    for( uint32_t nodeIterator = 0; nodeIterator < graph->nodeCount; ++nodeIterator )
    {
        if( !StringCompare( name, graph->nodes[nodeIterator].name ) )
        {
            return &graph->nodes[nodeIterator];
        }
    }

    // TODO: Handle NodeNotFound WARNING
    return 0;
}

static inline void Visit( struct Node* node )
{
    PrintNode( node );
}

void DepthFirstTraversalFromNode( struct Node* node )
{
    Visit( node );
    node->visited = TRUE;

    for( uint32_t nodeIterator = 0; nodeIterator < node->adjacentNodeCount; ++nodeIterator )
    {
        if( !node->adjacentNodes[nodeIterator].visited )
        {
            DepthFirstTraversalFromNode( &node->adjacentNodes[nodeIterator] );
        }
    }
}

void DepthFirstTraversalOfGraph( struct Graph* graph )
{
    printf("Depth first traversal of graph '%s'\n", graph->name);
    if( graph->nodeCount ) // Ensure there are nodes in graph
    {
        DepthFirstTraversalFromNode( &graph->nodes[0] );
    }
}

// TODO: The queue I currently have implemented does not support a "Node" type
//          I can either add it or do a makeshift queue struct within this file, having a queue for Node type may be useful
void BreadthFirstTraversalFromNode( struct Node* node )
{
}

void BreadthFirstTraversalOfGraph( struct Graph* graph )
{
    if( graph->nodeCount )
    {
        BreadthFirstTraversalFromNode( &graph->nodes[0] );
    }
}

void PrintGraph( struct Graph* graph )
{
    printf("%i node(s) in graph %s. The adjacency list: \n", graph->nodeCount, graph->name);
    for( uint32_t nodeIterator = 0; nodeIterator < graph->nodeCount; ++nodeIterator )
    {
        PrintNode( &graph->nodes[nodeIterator] );
    }
}

void DestroyGraph( struct Graph* graph )
{
    for( uint32_t nodeIterator = 0; nodeIterator < graph->nodeCount; ++nodeIterator )
    {
        DestroyNode( &graph->nodes[nodeIterator] );
    }

    free( graph->nodes );
    graph->nodes = 0;

    graph->nodeCount = 0;
}







// Tests

void VerifyCreateNode()
{
    struct Node node1 = CreateNode( "node1", 0, 0 );
    struct Node node2 = CreateNode( "node2", 0, 0 );
    struct Node node3 = CreateNode( "node3", 0, 0 );

    struct Node node = CreateNode( "node", 0, 0 );

    AddAdjacentNodeToNode( &node1, &node );
    AddAdjacentNodeToNode( &node2, &node );

    AddAdjacentNodeToNode( &node3, &node );
    AddAdjacentNodeToNode( &node, &node3 );

    PrintNode( &node );
    PrintNode( &node3 );
}

void VerifyCreateGraph()
{
    CreateGraph( 0, DISCONNECTED_DIGRAPH );
    CreateGraph( 0, ACYCLIC );
    CreateGraph( 0, UNDIRECTED );
    CreateGraph( 0, WEAKLY_CONNECTED );
    CreateGraph( 0, STRONGLY_CONNECTED );
}

void VerifyCreateAndAddNodeToGraph( struct Graph* graph )
{
    CreateAndAddNodeToGraph( "Test", 0, 0, graph );

    PrintGraph( graph );
}

void VerifyFindNodeByNameInGraph()
{
    struct Graph graph = CreateGraph( "testGraph", DISCONNECTED_DIGRAPH );

    struct Node node1 = CreateAndAddNodeToGraph( "node1", 0, 0, &graph );
    struct Node node2 = CreateAndAddNodeToGraph( "node2", 0, 0, &graph );
    AddAdjacentNodeToNodeInGraph( &node2, &node1, &graph );
    
    struct Node node = *FindNodeByNameInGraph( "node1", &graph );

    if( node.name )
        PrintNode( &node );

    DestroyGraph( &graph );
}

void VerifyDepthFirstTraversal( struct Graph* graph )
{
    DepthFirstTraversalOfGraph( graph );
}

void VerifyGraph( enum GraphType type )
{
    VerifyCreateNode();

    VerifyCreateGraph();

    struct Graph graph = CreateGraph( "testGraph", type );

    VerifyCreateAndAddNodeToGraph( &graph );

    VerifyFindNodeByNameInGraph();

    PrintGraph( &graph );

    VerifyDepthFirstTraversal( &graph );

    DestroyGraph( &graph );
}


