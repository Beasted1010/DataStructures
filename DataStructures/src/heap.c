
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "heap.h"

static inline void PrintArray( int* array, int arrayLength )
{
    for( int i = 0; i < arrayLength; i++ )
    {
        printf("%i", array[i]);

        if( i < arrayLength - 1 )
            printf(",");
    }
    printf("\n");
}


// Helpers
static inline int ParentIndex( int index )
{
    return index >> 1;
}

static inline int LeftChildIndex( int index )
{
    return index << 1;
}

static inline int RightChildIndex( int index )
{
    return (index << 1) + 1;
}

static inline void Swap( int first, int second, int* array )
{
    int temp = array[first];
    array[first] = array[second];
    array[second] = temp;
}



// Heap Functions

struct Heap CreateMaxHeap( int* array, int arrayLength )
{
    struct Heap heap;
    heap.heapSize = arrayLength;
    heap.itemCount = arrayLength;

    heap.items = (int*) malloc( sizeof(int) * heap.itemCount );
    memcpy( (void*) heap.items, (void*) array, sizeof(int) * arrayLength );

    // Heap property maintained at the leaf nodes (since there are no children).
    //  We therefore want to start at the level of nodes above the leav nodes.
    // Since a binary tree, each level adds 2x the nodes,
    //  so to get rid of last lay of nodes (leaf nodes), divide by 2 (>> 1)
    for( int i = heap.itemCount >> 1; i >= 0; i-- )
        MaxHeapify( i, heap.items, heap.itemCount );

    return heap;
}

void MaxHeapify( int index, int* array, int arrayLength )
{
    int left = LeftChildIndex( index );
    int right = RightChildIndex( index );

    int largest = index;

    if( left < arrayLength && array[left] > array[index] )
        largest = left;
    
    if( right < arrayLength && array[right] > array[largest] )
        largest = right;

    if( largest != index )
    {
        Swap( index, largest, array );
        MaxHeapify( largest, array, arrayLength );
    }

    return;
}

void MaxHeapSort( int* array, int arrayLength )
{
    struct Heap heap = CreateMaxHeap( array, arrayLength );

    // NOTE: I could stop at second to last element since that element is known to be the max, but I want to also copy all entires back to array
    for( int i = 0; i < heap.itemCount; i++ )
    {
        array[i] = heap.items[0]; // Front of heap array is guaranteed to be max, place this in the next location of the resulting array

        Swap( 0, heap.heapSize-1, heap.items ); // First element in our max heap will always be the max, move this to end
        heap.heapSize--;
        MaxHeapify( 0, heap.items, heap.heapSize );
    }
}

void DestroyHeap( struct Heap* heap )
{
    heap->itemCount = 0;
    heap->heapSize = 0;

    free( heap->items );
}

void PrintHeap( struct Heap* heap )
{
    printf("Heap: ");
    PrintArray( heap->items, heap->itemCount );
}




// Tests

void VerifyCreateMaxHeap( int* array, int arrayLength )
{
    LOG_VERIFY("CreateMaxHeap");

    printf("Before...\n");
    PrintArray( array, arrayLength );

    struct Heap max_heap = CreateMaxHeap( array, arrayLength );

    printf("After...\n");
    PrintArray( max_heap.items, max_heap.itemCount );

    DestroyHeap( &max_heap );
}

void VerifyHeapSort( int* array, int arrayLength )
{
    LOG_VERIFY("HeapSort");

    printf("Before...\n");
    PrintArray( array, arrayLength );

    MaxHeapSort( array, arrayLength );

    printf("After...\n");
    PrintArray( array, arrayLength );
}







