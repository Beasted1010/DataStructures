

#include <time.h>
#include <stdlib.h>

#include "array_builder.h"


void FillRandomIntArrayInRange( int* array, uint32_t arrayLength, int min, int max )
{
    srand( time(0) );

    for( int i = 0; i < arrayLength; i++ )
    {
        array[i] = ( rand() % (max - min + 1) ) + min;
    }
}

void FillRandomIntArray( int* array, uint32_t arrayLength )
{
    FillRandomIntArrayInRange( array, arrayLength, 0, RAND_MAX );
}



