
#ifndef ALGORITHMS_H
#define ALGORITHMS_H

#ifdef __cplusplus
extern "C" {
#endif


// TODO: Include directory containing DataStructures (path need added to makefile)


// ----------------------- Utilities -----------------------
void NewLine();
void PrintArray( int* array, int length );



// ----------------------- Algorithms -----------------------
void QuickSort( int* array, int arrayLength );


#ifdef __cplusplus
}
#endif

#endif

