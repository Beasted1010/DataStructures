
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "algorithms.h"

// ----------------------- Utilities -----------------------

void Swap( int* index1, int* index2, int* array )
{
    int temp = array[*index1];
    array[*index1] = array[*index2];
    array[*index2] = temp;
}

void NewLine()
{
    printf("\n");
}

void PrintArray( int* array, int length )
{
    printf("[");
    for( int i = 0; i < length; i++ )
    {
        printf("%i", array[i]);

        if( i < length - 1 ) // If before last iteration
            printf(", ");
        else
            printf("]\n");
    }
}




// -------------------- QUICK SORT -------------------- 

// Correctly places pivot and returns pivot index
int Partition( int* array, int leftIndex, int rightIndex )
{
    int pivot = array[rightIndex];

    int less_than_boundary_index = leftIndex - 1; // May start at -1

    for( int current_index = leftIndex; current_index <= rightIndex - 1; 
                                        current_index++ )
    {
        if( array[current_index] <= pivot )
        {
            less_than_boundary_index++;
            Swap( &current_index, &less_than_boundary_index, array );
        }
    }

    less_than_boundary_index++;
    Swap( &rightIndex, &less_than_boundary_index, array );

    return less_than_boundary_index;
}

// An optimized version of Partition (since helps ensure hardly ever worst case)
int RandomizedPartition( int* array, int leftIndex, int rightIndex )
{
    int random_index = (rand() % (rightIndex - leftIndex + 1) ) + leftIndex;
    Swap( &random_index, &rightIndex, array );

    return Partition( array, leftIndex, rightIndex );
}

void _QuickSort( int* array, int leftIndex, int rightIndex )
{
    if( leftIndex >= rightIndex )
        return;

    int pivot_index = Partition( array, leftIndex, rightIndex );

    _QuickSort( array, leftIndex, pivot_index - 1 ); // Left half
    _QuickSort( array, pivot_index + 1, rightIndex );

    return;
}

void _RandomizedQuickSort( int* array, int leftIndex, int rightIndex )
{
    if( leftIndex >= rightIndex )
        return;

    int pivot_index = RandomizedPartition( array, leftIndex, rightIndex );

    _RandomizedQuickSort( array, leftIndex, pivot_index - 1 ); // Left half
    _RandomizedQuickSort( array, pivot_index + 1, rightIndex );

    return;
}

void QuickSort( int* array, int arrayLength )
{
    _QuickSort( array, 0, arrayLength - 1 );
}

void RandomizedQuickSort( int* array, int arrayLength )
{
    srand( time(NULL) );
    _RandomizedQuickSort( array, 0, arrayLength - 1 );
}


// -------------------- ---------- -------------------- 



