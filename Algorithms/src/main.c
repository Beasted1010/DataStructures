
#include <stdio.h>

#include "algorithms.h"



int main( int argc, char** argv )
{
    int array[] = { 4, 2, 1, 5, 6, 8, 3 };
    int array_length = sizeof(array) / sizeof(array[0]);

    printf("Before sorting...\n");
    PrintArray( array, array_length );

    QuickSort( array, array_length );
    NewLine();

    printf("After sorting...\n");
    PrintArray( array, array_length );

    return 0;
}





