
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>

void QuickSort( int* array, int arrayLength );
void RandomizedQuickSort( int* array, int arrayLength );

char ToUpper( char c );
char ToLower( char c );
int Exponentiate( int base, int exponent );
void NewLine();
void PrintArray( int* array, int length );
void PrintArrayToFile( int* array, int length, FILE* fp );


void BuildArrayWithSortedIncreasingOrderValues( int* array, int arrayLength, 
                                                uint32_t incrementedAmount );
void BuildArrayWithRandomValues( int* array, int arrayLength );


void RunQuickSortNTimesWithRandomArray( int iterationCountN, 
                                        const int* inputSizes, 
                                        int inputSizesCount );
void RunRandomizedQuickSortNTimesWithRandomArray( int iterationCountN, 
                                                  const int* inputSizes, 
                                                  int inputSizesCount );
void RunQuickSortNTimesWithSortedArray( int iterationCountN, 
                                        const int* inputSizes, 
                                        int inputSizesCount, 
                                        uint32_t incrementedAmount );
void RunRandomizedQuickSortNTimesWithSortedArray( int iterationCountN, 
                                                  const int* inputSizes, 
                                                  int inputSizesCount, 
                                                  uint32_t incrementedAmount );

struct Profiler
{
    clock_t startTime, stopTime;
    double timeTaken;
};

void InitializeProfiler( struct Profiler* profiler );
void StartProfiler( struct Profiler* profiler );
void StopProfiler( struct Profiler* profiler );
void PrintProfilerTimeTakenForActivity( struct Profiler* profiler, 
                                        const char* activity );
void PrintProfilerTimeTakenForActivityToFile( FILE* fp, struct Profiler* profiler, 
                                              const char* activity );

#define MIN_RANDOM_RANGE 0
#define MAX_RANDOM_RANGE 10000

#define OUTPUT_FILE "output.txt"

#define COMPUTE_AVERAGE_RANDOMIZED_QUICK_SORT_RANDOM_ARRAY        1
#define COMPUTE_AVERAGE_QUICK_SORT_RANDOM_ARRAY                   1

#define COMPUTE_AVERAGE_RANDOMIZED_QUICK_SORT_SORTED_ARRAY        1
#define COMPUTE_AVERAGE_QUICK_SORT_SORTED_ARRAY                   1

int main( int argc, char** argv )
{
    struct Profiler profiler;
    InitializeProfiler( &profiler );

    const int sizes_of_arrays[] = { 10, 100, 1000, 10000 };

    int array_length;
    printf("Please enter in the size of an array to sort: ");
    scanf("%i%*c", &array_length);

    int* array = malloc( sizeof(int) * array_length );

    uint8_t valid_choice = 0;
    while( !valid_choice )
    {
        char choice;
        printf("How would you like the integers for this array chosen? (enter in a letter)\n");
        printf(" a. Sort in increasing order\n");
        printf(" b. Pick randomly from a uniform distribution\n");
        scanf("%c%*c", &choice);
        choice = ToUpper( choice );

        valid_choice = 1;
        switch( choice )
        {
            case 'A':
            {
                uint32_t incremented_amount;
                printf("Please enter the value to increment each element by: ");
                scanf("%i%*c", &incremented_amount);

                BuildArrayWithSortedIncreasingOrderValues( array, array_length,
                                                           incremented_amount );
            } break;

            case 'B':
            {
                BuildArrayWithRandomValues( array, array_length );
            } break;

            default:
            {
                valid_choice = 0;
                printf("Invalid choice, please try again\n\n");
            } break;
        }
    }



#if COMPUTE_AVERAGE_QUICK_SORT_RANDOM_ARRAY
    RunQuickSortNTimesWithRandomArray( 
            5, sizes_of_arrays, 
            sizeof(sizes_of_arrays) / sizeof(sizes_of_arrays[0]) );
#endif
#if COMPUTE_AVERAGE_RANDOMIZED_QUICK_SORT_RANDOM_ARRAY
    RunRandomizedQuickSortNTimesWithRandomArray( 
            5, sizes_of_arrays, 
            sizeof(sizes_of_arrays) / sizeof(sizes_of_arrays[0]) );
#endif

#if COMPUTE_AVERAGE_QUICK_SORT_SORTED_ARRAY
    RunQuickSortNTimesWithSortedArray( 
            5, sizes_of_arrays, 
            sizeof(sizes_of_arrays) / sizeof(sizes_of_arrays[0]), 1 );
#endif
#if COMPUTE_AVERAGE_RANDOMIZED_QUICK_SORT_SORTED_ARRAY
    RunRandomizedQuickSortNTimesWithSortedArray( 
            5, sizes_of_arrays, 
            sizeof(sizes_of_arrays) / sizeof(sizes_of_arrays[0]), 1 );
#endif




    FILE* fp = fopen( (const char*) OUTPUT_FILE, "w" );

    fprintf(fp, "--------- Before sorting...\n");
    PrintArrayToFile( array, array_length, fp );

    StartProfiler( &profiler );
    QuickSort( array, array_length );
    StopProfiler( &profiler );

    printf("\nTime taken for user provided array information...\n");
    PrintProfilerTimeTakenForActivity( &profiler, "Sorting");
    printf("See output file %s for details\n", OUTPUT_FILE);

    fprintf(fp, "\n--------- After sorting...\n");
    PrintArrayToFile( array, array_length, fp );
    printf("\n");

    PrintProfilerTimeTakenForActivityToFile( fp, &profiler, "Sorting...\n\n");

    fclose( fp );
    free( array );

    return 0;
}

// ----------------------- Utilities -----------------------

static inline void PrintMessageForAverageComputation( const char* algorithmName,
                                                      int iterationCountN,
                                                      const int* inputSizes,
                                                      int inputSizesCount )
{
    printf("\n------COMPUTING AVERAGE FOR %s------\n", algorithmName);
    printf("Using %i iterations with [", iterationCountN);
    for( int i = 0; i < inputSizesCount; i++ )
    {
        printf("%i", inputSizes[i]);
        
        if( i < inputSizesCount - 1 )
            printf(", ");
    }
    printf("] array sizes\n");
}

void RunQuickSortNTimesWithSortedArray( int iterationCountN, 
                                        const int* inputSizes, 
                                        int inputSizesCount, 
                                        uint32_t incrementedAmount )
{
    PrintMessageForAverageComputation( "QuickSort with Sorted Array", 
                                       iterationCountN, 
                                       inputSizes, inputSizesCount);

    if( iterationCountN <= 0 )
    {
        printf("Iteration count (currently %i) must be a positive integer\n", iterationCountN);
        return;
    }
    if( !inputSizes )
    {
        printf("Input sizes must not be null!\n");
        return;
    }
    if( inputSizesCount <= 0 )
    {
        printf("The number of input sizes (currently %i) must be a positive number that matches the size of the inputSizes array!\n", inputSizesCount);
        return;
    }

    struct Profiler profiler;
    InitializeProfiler( &profiler );

    double average_runtime;
    int array_length;

    for( int j = 0; j < inputSizesCount; j++ )
    {
        average_runtime = 0;

        array_length = inputSizes[j];
        int* array = malloc( sizeof(int) * array_length );

        for( int i = 0; i < iterationCountN; i++ )
        {
            BuildArrayWithSortedIncreasingOrderValues( array, array_length, 
                                                       incrementedAmount );

            StartProfiler( &profiler );
            QuickSort( array, array_length );
            StopProfiler( &profiler );
            PrintProfilerTimeTakenForActivity( &profiler, "Sorting");
            average_runtime += profiler.timeTaken;
        }

        average_runtime = average_runtime / iterationCountN;
        printf("AVERAGE TIME TAKEN.... IS............... %f\n", average_runtime);

        free( array );
    }

}

void RunRandomizedQuickSortNTimesWithSortedArray( int iterationCountN, 
                                                  const int* inputSizes, 
                                                  int inputSizesCount, 
                                                  uint32_t incrementedAmount )
{
    PrintMessageForAverageComputation( "Randomized QuickSort with Sorted Array", 
                                       iterationCountN, 
                                       inputSizes, inputSizesCount);

    if( iterationCountN <= 0 )
    {
        printf("Iteration count (currently %i) must be a positive integer\n", iterationCountN);
        return;
    }
    if( !inputSizes )
    {
        printf("Input sizes must not be null!\n");
        return;
    }
    if( inputSizesCount <= 0 )
    {
        printf("The number of input sizes (currently %i) must be a positive number that matches the size of the inputSizes array!\n", inputSizesCount);
        return;
    }

    struct Profiler profiler;
    InitializeProfiler( &profiler );

    double average_runtime;
    int array_length;

    for( int j = 0; j < inputSizesCount; j++ )
    {
        average_runtime = 0;

        array_length = inputSizes[j];
        int* array = malloc( sizeof(int) * array_length );

        for( int i = 0; i < iterationCountN; i++ )
        {
            BuildArrayWithSortedIncreasingOrderValues( array, array_length, 
                                                       incrementedAmount );

            StartProfiler( &profiler );
            RandomizedQuickSort( array, array_length );
            StopProfiler( &profiler );
            PrintProfilerTimeTakenForActivity( &profiler, "Sorting");
            average_runtime += profiler.timeTaken;
        }

        average_runtime = average_runtime / iterationCountN;
        printf("AVERAGE TIME TAKEN.... IS............... %f\n", average_runtime);

        free( array );
    }
}

void RunQuickSortNTimesWithRandomArray( int iterationCountN, 
                                        const int* inputSizes, 
                                        int inputSizesCount )
{
    PrintMessageForAverageComputation( "QuickSort with Random Array", 
                                       iterationCountN, 
                                       inputSizes, inputSizesCount);

    if( iterationCountN <= 0 )
    {
        printf("Iteration count (currently %i) must be a positive integer\n", iterationCountN);
        return;
    }
    if( !inputSizes )
    {
        printf("Input sizes must not be null!\n");
        return;
    }
    if( inputSizesCount <= 0 )
    {
        printf("The number of input sizes (currently %i) must be a positive number that matches the size of the inputSizes array!\n", inputSizesCount);
        return;
    }

    struct Profiler profiler;
    InitializeProfiler( &profiler );

    double average_runtime;
    int array_length;

    for( int j = 0; j < inputSizesCount; j++ )
    {
        average_runtime = 0;

        array_length = inputSizes[j];
        int* array = malloc( sizeof(int) * array_length );

        for( int i = 0; i < iterationCountN; i++ )
        {
            BuildArrayWithRandomValues( array, array_length );

            StartProfiler( &profiler );
            QuickSort( array, array_length );
            StopProfiler( &profiler );
            PrintProfilerTimeTakenForActivity( &profiler, "Sorting");
            average_runtime += profiler.timeTaken;
        }

        average_runtime = average_runtime / iterationCountN;
        printf("AVERAGE TIME TAKEN.... IS............... %f\n", average_runtime);

        free( array );
    }
}

void RunRandomizedQuickSortNTimesWithRandomArray( int iterationCountN, 
                                                  const int* inputSizes, 
                                                  int inputSizesCount )
{
    PrintMessageForAverageComputation( "Randomized QuickSort with Random Array", 
                                       iterationCountN, 
                                       inputSizes, inputSizesCount);


    if( iterationCountN <= 0 )
    {
        printf("Iteration count (currently %i) must be a positive integer\n", iterationCountN);
        return;
    }
    if( !inputSizes )
    {
        printf("Input sizes must not be null!\n");
        return;
    }
    if( inputSizesCount <= 0 )
    {
        printf("The number of input sizes (currently %i) must be a positive number that matches the size of the inputSizes array!\n", inputSizesCount);
        return;
    }

    struct Profiler profiler;
    InitializeProfiler( &profiler );

    double average_runtime;
    int array_length;

    for( int j = 0; j < inputSizesCount; j++ )
    {
        average_runtime = 0;

        array_length = inputSizes[j];
        int* array = malloc( sizeof(int) * array_length );

        for( int i = 0; i < iterationCountN; i++ )
        {
            BuildArrayWithRandomValues( array, array_length );

            StartProfiler( &profiler );
            RandomizedQuickSort( array, array_length );
            StopProfiler( &profiler );
            PrintProfilerTimeTakenForActivity( &profiler, "Sorting");
            average_runtime += profiler.timeTaken;
        }

        average_runtime = average_runtime / iterationCountN;
        printf("AVERAGE TIME TAKEN.... IS............... %f\n", average_runtime);

        free( array );
    }
}

void BuildArrayWithSortedIncreasingOrderValues( int* array, int arrayLength, 
                                                uint32_t incrementedAmount )
{
    for( uint32_t i = 0; i < arrayLength; i++ )
    {
        array[i] = arrayLength + (i+1) * incrementedAmount;
    }
}

void BuildArrayWithRandomValues( int* array, int arrayLength )
{
    srand( time(NULL) );

    for( int i = 0; i < arrayLength; i++ )
    {
        array[i] = (rand() % ( (MAX_RANDOM_RANGE - MIN_RANDOM_RANGE) + 1 )) + MIN_RANDOM_RANGE;
    }
}

inline void InitializeProfiler( struct Profiler* profiler )
{
    profiler->startTime = 0;
    profiler->stopTime = 0;
    profiler->timeTaken = 0;
}

void StartProfiler( struct Profiler* profiler )
{
    profiler->timeTaken = 0.0;
    profiler->stopTime = 0;
    profiler->startTime = clock();
}

void StopProfiler( struct Profiler* profiler )
{
    if( !profiler->startTime )
        return;

    profiler->stopTime = clock();
    profiler->timeTaken = ( (double) profiler->stopTime - profiler->startTime ) / CLOCKS_PER_SEC;
}

void PrintProfilerTimeTakenForActivity( struct Profiler* profiler, 
                                        const char* activity )
{
    // In seconds
    printf("Time taken to do %s: %f seconds\n", activity, profiler->timeTaken);
}

void PrintProfilerTimeTakenForActivityToFile( FILE* fp, struct Profiler* profiler, 
                                              const char* activity )
{
    // In seconds
    fprintf(fp, "Time taken to do %s: %f seconds\n", activity, profiler->timeTaken);
}

char ToUpper( char c )
{
    return ( c >= 'a' ? c -= ('a' - 'A') : c );
}

char ToLower( char c )
{
    return ( c < 'a' ? c += ('a' - 'A') : c );
}

int Exponentiate( int base, int exponent )
{
    int result = 1;
    for(;;)
    {
        if( exponent & 1 ) // Odd
            result *= base;

        exponent >>= 1; // Divide exponent by 2

        if( !exponent )
            break;

        base *= base;
    }

    return result;
}

void Swap( int* index1, int* index2, int* array )
{
    int temp = array[*index1];
    array[*index1] = array[*index2];
    array[*index2] = temp;
}

void NewLine()
{
    printf("\n");
}

void PrintArray( int* array, int length )
{
    printf("[");
    for( int i = 0; i < length; i++ )
    {
        printf("%i", array[i]);

        if( i < length - 1 ) // If before last iteration
            printf(", ");
        else
            printf("]\n");
    }
}

void PrintArrayToFile( int* array, int length, FILE* fp )
{
    fprintf(fp, "[");
    for( int i = 0; i < length; i++ )
    {
        fprintf(fp, "%i", array[i]);

        if( i < length - 1 ) // If before last iteration
            fprintf(fp, ", ");
        else
            fprintf(fp, "]\n");
    }
}

// -------------------- QUICK SORT -------------------- 
// NOTE: Algorithm was created by program author and then tweaked based on the "Introduction to Algorithms" textbook by Thomas Cormen and colleagues.

// Correctly places pivot and returns pivot index
int Partition( int* array, int leftIndex, int rightIndex )
{
    int pivot = array[rightIndex];

    int less_than_boundary_index = leftIndex - 1; // May start at -1

    for( int current_index = leftIndex; current_index <= rightIndex - 1; 
                                        current_index++ )
    {
        if( array[current_index] <= pivot )
        {
            less_than_boundary_index++;
            Swap( &current_index, &less_than_boundary_index, array );
        }
    }

    less_than_boundary_index++;
    Swap( &rightIndex, &less_than_boundary_index, array );

    return less_than_boundary_index;
}

// An optimized version of Partition (since helps ensure hardly ever worst case)
int RandomizedPartition( int* array, int leftIndex, int rightIndex )
{
    int random_index = (rand() % (rightIndex - leftIndex + 1) ) + leftIndex;
    Swap( &random_index, &rightIndex, array );

    return Partition( array, leftIndex, rightIndex );
}

void _QuickSort( int* array, int leftIndex, int rightIndex )
{
    if( leftIndex >= rightIndex )
        return;

    int pivot_index = Partition( array, leftIndex, rightIndex );

    _QuickSort( array, leftIndex, pivot_index - 1 ); // Left half
    _QuickSort( array, pivot_index + 1, rightIndex ); // Right half
}

void _RandomizedQuickSort( int* array, int leftIndex, int rightIndex )
{
    if( leftIndex >= rightIndex )
        return;

    int pivot_index = RandomizedPartition( array, leftIndex, rightIndex );

    _RandomizedQuickSort( array, leftIndex, pivot_index - 1 ); // Left half
    _RandomizedQuickSort( array, pivot_index + 1, rightIndex ); // Right half
}

void QuickSort( int* array, int arrayLength )
{
    _QuickSort( array, 0, arrayLength - 1 );
}

void RandomizedQuickSort( int* array, int arrayLength )
{
    srand( time(NULL) );
    _RandomizedQuickSort( array, 0, arrayLength - 1 );
}



