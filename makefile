
OBJ=obj
LIB=lib

MK_LIB=ar rcs
LIB_NAME=fyestandardlibrary.a

SUBLIBRARY_DIRS = Algorithms/. DataStructures/. Functions/.

_SUBDIRS := $(wildcard */.)
SUBDIRS := $(filter $(SUBLIBRARY_DIRS),$(_SUBDIRS)) # Only keep SUBLIBRARIES

SUBLIBRARIES = $(patsubst %, %/$(LIB)/*, $(SUBDIRS))


PATH_TO_OBJECT_FOLDER = $(join $(SUBLIBRARY_DIRS),/$(OBJ))

# NOTE: This was a more complicated approach where I attempt to get a list of all individual object files (doesn't work yet), but I can just do /obj/*
#PATH_TO_ARCHIVES = $(join $(SUBLIBRARIES:*=),$(shell ar -t $(LIB)/lib$(LIB_NAME)))
#ALL_OBJECT_FILES = $(foreach ARCHIVE,$(PATH_TO_ARCHIVES),$(shell ar -t $(ARCHIVE)))

PATHS_TO_OBJECT_FOLDERS = $(foreach SUBLIBRARY_DIR,$(SUBLIBRARY_DIRS),$(SUBLIBRARY_DIR)/$(OBJ))

OBJECT_FILES = $(foreach PATH_TO_OBJECT_FOLDER,$(PATHS_TO_OBJECT_FOLDERS),$(wildcard $(PATH_TO_OBJECT_FOLDER:*=)/*.o))



all: $(SUBDIRS)
$(SUBDIRS):
	$(MAKE) -C $@


library: $(SUBDIRS)
	$(MK_LIB) $(LIB)/lib$(LIB_NAME) $(OBJECT_FILES)


.PHONY: all $(SUBDIRS)

.PHONY: help
help:
	$(info $$SUBLIBRARY_DIRS = [$(SUBLIBRARY_DIRS)])
	$(info $$SUBDIRS = [$(SUBDIRS)])
	$(info $$SUBLIBRARIES = [$(SUBLIBRARIES)])
	$(info $$PATHS_TO_OBJECT_FOLDERS = $(PATHS_TO_OBJECT_FOLDERS))
	$(info $$OBJECT_FILES = $(OBJECT_FILES))
